package deutschebank.dbutils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;

public class DBConnector {
    private static final Logger LOGGER = java.util.logging.Logger.getLogger(DBConnector.class.getName());

    private static Properties properties = initProperties();

    private DBConnector() {
    }


    public static Connection getConnection() {
        Connection itsConnection = null;
        try {
            LOGGER.info("On Entry -> DBConnector.connect()");

            String dbDriver = properties.getProperty("dbDriver");
            String dbPath = properties.getProperty("dbPath");
            String dbName = properties.getProperty("dbName");
            String dbUser = properties.getProperty("dbUser");
            String dbPwd = properties.getProperty("dbPwd");

            Class.forName(dbDriver);
            itsConnection = DriverManager.getConnection(dbPath + dbName,
                    dbUser,
                    dbPwd);

            LOGGER.info("Successfully connected to " + dbName);

        } catch (ClassNotFoundException | SQLException | NullPointerException e) {
            e.printStackTrace();
        }

        LOGGER.info("On Exit -> DBConnector.connect()");

        return itsConnection;
    }

    private static Properties initProperties() {
        PropertyLoader pLoader = PropertyLoader.getLoader();
        try {
            properties = pLoader.getPropValues("dbConnector.properties");
        } catch (IOException e) {
            LOGGER.info("Properties were not initialised");
        }
        return properties;
    }
}

