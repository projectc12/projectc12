package deutschebank.entity;

public class Extra3Entity {
    String dealerName;
    Float profitLoss;


    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public Float getProfitLoss() {
        return profitLoss;
    }

    public void setProfitLoss(Float profitLoss) {
        this.profitLoss = profitLoss;
    }
}
