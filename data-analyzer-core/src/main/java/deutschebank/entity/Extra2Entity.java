package deutschebank.entity;

public class Extra2Entity {
    private String counterparty;
    private String instrument;

    public Extra2Entity(String counterparty, String instrument, double net) {
        this.counterparty = counterparty;
        this.instrument = instrument;
        this.net = net;
    }

    private double net;

    public String getCounterparty() {
        return counterparty;
    }

    public void setCounterparty(String counterparty) {
        this.counterparty = counterparty;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public double getNet() {
        return net;
    }

    public void setNet(double net) {
        this.net = net;
    }
}
