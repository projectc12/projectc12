package deutschebank.entity;

public class Extra1Entity {
    String instrumentName;
    Float averageBuyPrice;
    Float averageSellPrice;

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public Float getAverageBuyPrice() {
        return averageBuyPrice;
    }

    public void setAverageBuyPrice(Float averageBuyPrice) {
        this.averageBuyPrice = averageBuyPrice;
    }

    public Float getAverageSellPrice() {
        return averageSellPrice;
    }

    public void setAverageSellPrice(Float averageSellPrice) {
        this.averageSellPrice = averageSellPrice;
    }
}
