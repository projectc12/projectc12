/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank.entity;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Selvyn
 */
@JsonPropertyOrder(alphabetic = true)
public class User {
    private String itsUserID;
    private String itsUserPwd;

    public User(String userid, String pwd) {
        itsUserID = userid;
        itsUserPwd = pwd;
    }

    public String getUserID() {
        return itsUserID;
    }

    public void setUserID(String itsUserID) {
        this.itsUserID = itsUserID;
    }

    public String getUserPwd() {
        return itsUserPwd;
    }

    public void setUserPwd(String itsUserPwd) {
        this.itsUserPwd = itsUserPwd;
    }


    public String toJSON() {
        String result = "<Some value from the server>";
        try {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(this);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(itsUserID, user.itsUserID) &&
                Objects.equals(itsUserPwd, user.itsUserPwd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itsUserID, itsUserPwd);
    }
}
