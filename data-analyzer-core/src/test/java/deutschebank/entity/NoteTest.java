package deutschebank.entity;

import org.junit.Assert;
import org.junit.Test;

public class NoteTest {

    @Test
    public void setDealId() {
        Note deal = new Note();
        deal.setDealId(1);
        Assert.assertEquals(1, deal.getDealId());
    }

    @Test
    public void getDealId() {
        Note deal = new Note();
        deal.setDealId(1);
        int dealId = deal.getDealId();
        Assert.assertEquals(1, dealId);
    }

    @Test
    public void setDealTime() {
        Note deal = new Note();
        deal.setDealTime("Happy Hour");
        Assert.assertEquals("Happy Hour", deal.getDealTime());
    }

    @Test
    public void getDealTime() {
        Note deal = new Note();
        deal.setDealTime("Happy Hour");
        String dealTime = deal.getDealTime();
        Assert.assertEquals("Happy Hour", dealTime);
    }

    @Test
    public void setDealType() {
        Note deal = new Note();
        deal.setDealType("Insider Trading");
        Assert.assertEquals("Insider Trading", deal.getDealType());
    }

    @Test
    public void getDealType() {
        Note deal = new Note();
        deal.setDealType("Insider Trading");
        String dealType = deal.getDealType();
        Assert.assertEquals("Insider Trading", dealType);
    }

    @Test
    public void setDealAmount() {
        Note deal = new Note();
        deal.setDealAmount(42.0);
        Assert.assertEquals(true, ((42.0 - deal.getDealAmount()) < 0.000001));
    }

    @Test
    public void getDealAmount() {
        Note deal = new Note();
        deal.setDealAmount(42.0);
        double dealAmount = deal.getDealAmount();
        Assert.assertEquals(true, ((42.0 - dealAmount) < 0.000001));
    }

    @Test
    public void setDealQuantity() {
        Note deal = new Note();
        deal.setDealQuantity(42);
        Assert.assertEquals(42, deal.getDealQuantity());
    }

    @Test
    public void getDealQuantity() {
        Note deal = new Note();
        deal.setDealQuantity(42);
        int dealQuantity = deal.getDealQuantity();
        Assert.assertEquals(42, dealQuantity);
    }

    @Test
    public void getInstrumentName() {
        Note deal = new Note();
        deal.setInstrumentName("Saxophone");
        String instrumentName = deal.getInstrumentName();
        Assert.assertEquals("Saxophone", instrumentName);
    }

    @Test
    public void setInstrumentName() {
        Note deal = new Note();
        deal.setInstrumentName("Saxophone");
        Assert.assertEquals("Saxophone", deal.getInstrumentName());
    }

    @Test
    public void getCounterpartyName() {
        Note deal = new Note();
        deal.setCounterpartyName("party");
        String counterpartyName = deal.getCounterpartyName();
        Assert.assertEquals("party", counterpartyName);
    }

    @Test
    public void setCounterpartyName() {
        Note deal = new Note();
        deal.setCounterpartyName("party");
        Assert.assertEquals("party", deal.getCounterpartyName());
    }

    @Test
    public void getCounterpartyStatus() {
        Note deal = new Note();
        deal.setCounterpartyStatus("Rich");
        String counterpartyStatus = deal.getCounterpartyStatus();
        Assert.assertEquals("Rich", counterpartyStatus);
    }

    @Test
    public void setCounterpartyStatus() {
        Note deal = new Note();
        deal.setCounterpartyStatus("Rich");
        Assert.assertEquals("Rich", deal.getCounterpartyStatus());
    }

    @Test
    public void getCounterpartyDateRegistered() {
        Note deal = new Note();
        deal.setCounterpartyDateRegistered("Some Date");
        String counterpartyDateRegistered = deal.getCounterpartyDateRegistered();
        Assert.assertEquals("Some Date", counterpartyDateRegistered);
    }

    @Test
    public void setCounterpartyDateRegistered() {
        Note deal = new Note();
        deal.setCounterpartyDateRegistered("Some Date");
        Assert.assertEquals("Some Date", deal.getCounterpartyDateRegistered());
    }
}