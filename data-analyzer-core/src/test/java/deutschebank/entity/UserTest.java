package deutschebank.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserTest {

    private User admin;

    @Before
    public void setUp() {
        admin = new User("admin", "admin");
    }

    @Test
    public void getUserID() {
        Assert.assertEquals(admin.getUserID(), "admin");
    }

    @Test
    public void setUserID() {
        admin.setUserID("not admin");
        Assert.assertEquals(admin.getUserID(), "not admin");
    }

    @Test
    public void getUserPwd() {
        Assert.assertEquals(admin.getUserPwd(), "admin");
    }

    @Test
    public void setUserPwd() {
        admin.setUserPwd("lame password");
        Assert.assertEquals(admin.getUserPwd(), "lame password");
    }

    @Test
    public void toJSON() {
        String expected = "{\"userID\":\"admin\",\"userPwd\":\"*****\"}";
        Assert.assertEquals(expected, admin.toJSON());
    }

    @Test
    public void equals() {
        deutschebank.entity.User user1 = new User("name", "pwd");
        deutschebank.entity.User user2 = new User("name", "pwd");
        Assert.assertEquals(user1, user2);
    }

    @Test
    public void getUserIdTest() {
        User user = new User("", "");
        String userId = user.getUserID();
        Assert.assertEquals("", userId);
    }

    @Test
    public void getUserPwdTest() {
        User user = new User("", "");
        String userPwd = user.getUserPwd();
        Assert.assertEquals("", userPwd);
    }

    @Test
    public void setUserIdTest() {
        User user = new User("", "");
        user.setUserID("234");
        Assert.assertEquals("234", user.getUserID());
    }

    @Test
    public void setUserPwdTest() {
        User user = new User("", "");
        user.setUserPwd("somepassword");
        Assert.assertEquals("somepassword", user.getUserPwd());
    }

}