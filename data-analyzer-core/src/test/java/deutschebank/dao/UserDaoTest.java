package deutschebank.dao;

import deutschebank.dbutils.DBConnector;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static junit.framework.TestCase.assertNotNull;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;


@RunWith(PowerMockRunner.class)
@PrepareForTest({DBConnector.class})
public class UserDaoTest {

    @Mock
    Connection connectionMock;

    @Mock
    PreparedStatement statementMock;

    @Before
    public void setUp() throws Exception {
        statementMock = mock(PreparedStatement.class);
        doNothing().when(statementMock).setString(anyInt(), anyString());

        connectionMock = mock(Connection.class);
        when(connectionMock.prepareStatement(Matchers.anyString())).thenReturn(statementMock);

        mockStatic(DBConnector.class);
        when(DBConnector.getConnection()).thenReturn(connectionMock);
    }

    @Test
    public void getConnection() {
        UserDao dao = new UserDao();
        Connection connection = dao.getConnection();
        assertNotNull(connection);
    }

    @Test
    public void prepareStatement() throws SQLException {
        assertNotNull(new UserDao().prepareStatement("", "", connectionMock));
    }


}