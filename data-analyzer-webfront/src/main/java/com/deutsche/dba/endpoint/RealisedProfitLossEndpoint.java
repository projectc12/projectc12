package com.deutsche.dba.endpoint;

import deutschebank.dao.UserDao;
import deutschebank.entity.Extra3Entity;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/extra/rpl")
public class RealisedProfitLossEndpoint {

    private UserDao userDao = new UserDao();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public List<Extra3Entity> doPost() {
        List<Extra3Entity> list = userDao.getProfitLoss();
        //TODO: fix hack
        list.add(0, list.get(0));

        return userDao.getProfitLoss();
    }
}
