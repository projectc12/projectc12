package com.deutsche.dba.endpoint;

import deutschebank.dao.UserDao;
import deutschebank.entity.Extra1Entity;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/extra/avgprices")
public class AvgPricesEndpoint {

    private UserDao userDao = new UserDao();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public List<Extra1Entity> getAverageForEveryInstrument() {
        List<Extra1Entity> list = userDao.getAveragePrices();

        //TODO: fix hack
        list.add(0, list.get(0));

        return list;
    }
}
