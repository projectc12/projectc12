package com.deutsche.dba.endpoint;

import com.deutsche.dba.services.UserService;
import deutschebank.entity.User;

import javax.servlet.ServletContext;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

@Path("/login")
public class LoginEndpoint {

    @Context
    ServletContext context;

    private UserService userService = new UserService();

    void setUserService(UserService us) {
        userService = us;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response doPost(@FormParam("name") String usr, @FormParam("password") String pwd) throws URISyntaxException {
        User user = userService.findUserByUserIdAndPwd(usr, pwd);

        if (user != null) {
            context.setAttribute("username", user.getUserID());
            context.setAttribute("password", user.getUserPwd());
            user.setUserPwd("******");
            return Response.temporaryRedirect(new URI("/dbanalyzer-c1-2/tables.jsp")).build();
        }
        return Response.status(403).build();
    }
}
