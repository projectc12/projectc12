package com.deutsche.dba.endpoint;

import deutschebank.dao.UserDao;
import deutschebank.entity.Extra2Entity;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/extra/end")
public class EndPositionsEndpoint {

    private UserDao userDao = new UserDao();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public List<Extra2Entity> doPost() {
        List<Extra2Entity> list = userDao.getEndPositions();
        //TODO: fix hack
        list.add(0, list.get(0));

        return list;
    }
}
