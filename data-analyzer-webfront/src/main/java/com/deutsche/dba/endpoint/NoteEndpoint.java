package com.deutsche.dba.endpoint;

import com.deutsche.dba.services.NoteService;
import deutschebank.entity.Note;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/notes/{offset}/{limit}")
public class NoteEndpoint {
    private NoteService noteService = new NoteService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response doPost(@PathParam("offset") int offset, @PathParam("limit") int limit) {
        List<Note> notePage = noteService.findNotesByOffsetAndLimit(offset, limit);

        //TODO: fix hack
        notePage.add(0, notePage.get(0));

        Response response = Response.status(200).entity(notePage).build();
        return response;
    }
}
