package com.deutsche.dba.endpoint;

import deutschebank.dbutils.DBConnector;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/checkConnection")
public class DBConnectionEndpoint {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public boolean checkConnection() {
        boolean isDbAvailable = DBConnector.getConnection() != null;
        return isDbAvailable;
    }

}
