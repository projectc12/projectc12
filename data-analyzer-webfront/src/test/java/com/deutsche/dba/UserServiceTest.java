package com.deutsche.dba;

import deutschebank.dao.UserDao;
import deutschebank.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    @Mock
    UserDao daoMock;

    @Before
    public void setUp() {
        daoMock = mock(UserDao.class);
        when(daoMock.loadFromDB(Matchers.anyString(), Matchers.anyString())).thenReturn(new User("", ""));
    }

    @Test
    public void findUserByUserIdAndPwd() {
        User user = daoMock.loadFromDB("", "");
        assertEquals(user, new User("", ""));
    }

}