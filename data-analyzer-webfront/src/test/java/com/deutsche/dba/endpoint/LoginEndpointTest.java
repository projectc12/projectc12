package com.deutsche.dba.endpoint;

import com.deutsche.dba.services.UserService;
import deutschebank.entity.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class LoginEndpointTest {

    @Mock
    UserService userServiceMock;

    private LoginEndpoint loginEndpoint;
    private Response succ = Response.temporaryRedirect(new URI("/dbanalyzer-c1-2/tables.jsp")).build();
    private Response fail = Response.status(403).build();

    public LoginEndpointTest() throws URISyntaxException {
    }

    @Before
    public void setUp() {
        User user = new User("admin", "admin");

        userServiceMock = mock(UserService.class);
        when(userServiceMock.findUserByUserIdAndPwd(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        when(userServiceMock.findUserByUserIdAndPwd("admin", "admin")).thenReturn(user);

        loginEndpoint = new LoginEndpoint();
        loginEndpoint.setUserService(userServiceMock);
    }

//    @Test
//    public void doPost_successful() throws URISyntaxException {
//        String userName = "admin";
//        String password = "admin";
//        Assert.assertEquals(succ, loginEndpoint.doPost(userName, password));
//    }
//
//    @Test
//    public void doPost_unsuccessful() throws URISyntaxException {
//        String userName = "admin";
//        String password = "wrong pwd";
//        Assert.assertEquals(fail, loginEndpoint.doPost(userName, password));
//    }
}