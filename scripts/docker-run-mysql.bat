echo off

set dir="C:/vm_share/mysql"
IF exist %dir% ( echo %dir% exists ) ELSE ( mkdir %dir% && echo %dir% created)

echo Starting MySQL, root password=ppp

set mysql_port=3307
if not %1=="" ( set mysql_port=%1)

set cont_name=mysql-dbda
if not %2=="" ( set cont_name=%2)

docker run -p %mysql_port%:3306 --name %cont_name% -e MYSQL_ROOT_PASSWORD=ppp -d -v C:\vm_share\mysql:/root mysql-db-unpopulated:latest
docker cp init-populate-mysql-db.sh %cont_name%:/root
docker cp db_grad_20180731.sql %cont_name%:/root
docker cp create_normalized_db.sql %cont_name%:/root
docker cp populate_normalized_db.sql %cont_name%:/root
echo MySQL container now running
echo 

echo Waiting for MySQL daemon to initialise, do not interrupt... (CTRL+C will terminate the install)
timeout 10 /NOBREAK

echo Initialising and populating the DB, can take approximately 5 minutes to complete, please wait...
docker exec %cont_name% bash -c "cd /root; ./init-populate-mysql-db.sh"
echo MySQL available for use...
echo  
