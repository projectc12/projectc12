echo off

set dir="C:/vm_share/tomcat-webapps"
IF exist %dir% ( echo %dir% exists ) ELSE ( mkdir %dir% && echo %dir% created)

set tomcat_port_1=32771
set tomcat_port_2=32772
if not "%1"=="" ( set tomcat_port_1=%1)
if not "%2"=="" ( set tomcat_port_2=%2)

echo Starting tomcat containers, please wait...
docker run -p %tomcat_port_1%:8080 -dit --name tomcat-1 -v C:\vm_share\tomcat-webapps:/usr/local/tomcat/webapps tomcat9-server
docker run -p %tomcat_port_2%:8080 -dit --name tomcat-2 -v C:\vm_share\tomcat-webapps:/usr/local/tomcat/webapps tomcat9-server

echo ..
echo ..
echo tomcat should now be available
echo