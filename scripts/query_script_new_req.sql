-- requirement 1

DROP view if exists `ABS`;
create view ABS
as
SELECT b.instrument_name, b.buy_price, s.sell_price
        FROM (
        select instrument.instrument_id, instrument.instrument_name, avg(deal.deal_amount) as buy_price
        from deal
        inner join instrument
        on deal.deal_instrument_id = instrument.instrument_id 
         where deal_type = 'B'
         group by instrument_id) as b
        INNER JOIN (				
        select instrument.instrument_id, instrument.instrument_name, avg(deal.deal_amount) as sell_price
        from deal 
        inner join instrument
        on deal.deal_instrument_id = instrument.instrument_id 
                 where deal_type = 'S'
         group by instrument_id) as s
        on b.instrument_name = s.instrument_name;
        
        
           
        
-- requirement 2
DROP VIEW if exists `end_pos`;

create view end_pos
as
        SELECT s.counterparty_name, s.instrument_name, (b.b - s.b) as ending_position
        FROM (SELECT counterparty.counterparty_name, instrument.instrument_name, sum(deal.deal_quantity) as b
        from deal inner join counterparty
        on deal.deal_counterparty_id = counterparty.counterparty_id
        inner join instrument
        on deal.deal_instrument_id = instrument.instrument_id
        where deal_type = 'S' group by counterparty_name, instrument_name) as s
        INNER JOIN (
        SELECT counterparty.counterparty_name, instrument.instrument_name, sum(deal.deal_quantity) as b
        from deal inner join counterparty
        on deal.deal_counterparty_id = counterparty.counterparty_id
        inner join instrument
        on deal.deal_instrument_id = instrument.instrument_id
        where deal_type = 'B' group by counterparty_name, instrument_name) as b
        ON s.counterparty_name = b.counterparty_name and s.instrument_name = b.instrument_name;
        
        
        -- requirement 3 

DROP VIEW IF EXISTS `RPL`;

create view RPL
as
  SELECT t1.counterparty_name,
        (t1.b - t2.b) as r_PL
        FROM (SELECT counterparty.counterparty_name, sum(deal.deal_amount*deal.deal_quantity) as b
        from deal inner join counterparty
        on deal.deal_counterparty_id = counterparty.counterparty_id
        where deal_type = 'S' group by counterparty_name) as t1
        INNER JOIN (SELECT counterparty.counterparty_name, sum(deal.deal_amount*deal.deal_quantity) as b
        from deal inner join counterparty
        on deal.deal_counterparty_id = counterparty.counterparty_id where deal_type = 'B'
        group by counterparty_name) as t2
        ON t1.counterparty_name = t2.counterparty_name;
        
        
-- requirement 4
DROP VIEW IF EXISTS `DPL`;
DROP VIEW IF EXISTS `end_buy`;
DROP VIEW IF EXISTS `end_sell`;

create view end_buy
as
select d_time.deal_instrument_id, d_price.deal_amount as end_buy_price from 
(
(select deal_instrument_id, max(deal_time) as end_time from deal where deal_type = 'B' group by deal_instrument_id) as d_time
left join 
(select deal_instrument_id, deal_amount, deal_time from deal where deal_type = 'B') as d_price
on d_time.deal_instrument_id = d_price.deal_instrument_id and d_time.end_time = d_price.deal_time
);

create view end_sell
as
select d_time.deal_instrument_id, d_price.deal_amount as end_sell_price from 
(
(select deal_instrument_id, max(deal_time) as end_time from deal where deal_type = 'S' group by deal_instrument_id) as d_time
left join 
(select deal_instrument_id, deal_amount, deal_time from deal where deal_type = 'S') as d_price
on d_time.deal_instrument_id = d_price.deal_instrument_id and d_time.end_time = d_price.deal_time
);




DROP VIEW IF EXISTS `EPL`;

create view EPL
as
select b.counterparty_name, 
(r.r_PL + long_pos + short_pos) as effective_PL
 from(
(select end_pos.counterparty_name, sum(end_buy.end_buy_price*end_pos.ending_position) as long_pos from end_pos
left join instrument
on end_pos.instrument_name = instrument.instrument_name
inner join end_buy
on end_buy.deal_instrument_id = instrument.instrument_id
where end_pos.ending_position > 0
group by end_pos.counterparty_name) as b

left join
(select end_pos.counterparty_name, sum(end_sell.end_sell_price*end_pos.ending_position) as short_pos from end_pos
left join instrument
on end_pos.instrument_name = instrument.instrument_name
inner join end_sell
on end_sell.deal_instrument_id = instrument.instrument_id
where end_pos.ending_position < 0
group by end_pos.counterparty_name) as s
on b.counterparty_name = s.counterparty_name

left join 
(select counterparty_name, RPL.r_PL from RPL) as r
on r.counterparty_name = b.counterparty_name
);