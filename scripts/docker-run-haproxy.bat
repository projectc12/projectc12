echo off
set haproxy_port=32770
if not "%1"=="" ( haproxy_port="%1" )

docker run -p %haproxy_port%:8080 -dit --name haproxy haproxy
echo
