echo off

set cont_name=mysql-dbda
if not %1=="" ( set cont_name=%1)

docker stop %cont_name%
docker rm %cont_name%
echo
