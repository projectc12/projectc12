echo off

echo Cleaning up docker containers...
call cleanup-haproxy-runtime.bat
call cleanup-tomcat-runtime.bat
call cleanup-mysql-runtime.bat mysql-dbda
echo Done

echo Building projects...
call ../build.bat
echo Done

echo Deploying war...
call deploy-application.bat
echo Done

echo Preparing docker containers...
docker build -t haproxy -f haproxy_dockerfile .
call docker-run-haproxy.bat
call docker-run-tomcat9-with-application.bat 32771 32772
call docker-run-mysql.bat 3307 mysql-dbda
echo Done

echo
