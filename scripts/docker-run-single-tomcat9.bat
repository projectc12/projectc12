echo off
echo Starting tomcat container, can take approximately 1 minute to complete, please wait...
docker run -p %1:8080 -dit --name tomcat-0 -v C:\vm_share\tomcat-webapps\dbanalyzer.war:/usr/local/tomcat/webapps/dbanalyzer.war tomcat9-server

echo ..
echo ..
echo tomcat should now be available
echo
