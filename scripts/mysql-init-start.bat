xcopy /s/y .\dump.sql c:\vm_share\mysql
docker stop mysql-dbda
docker rm mysql-dbda
docker run -p 3306:3306 --name mysql-dbda -e MYSQL_ROOT_PASSWORD=ppp -d -v C:\vm_share\mysql:/root mysql-db-unpopulated:latest
docker cp init-populate-mysql-db.sh mysql-dbda:/root
docker cp dump.sql mysql-dbda:/root
timeout 10 /NOBREAK
docker exec -it mysql-dbda bash -c "cd /root; ./init-populate-mysql-db.sh"
del c:\vm_share\mysql\dump.sql
docker ps -a